# 編譯及使用方式

編譯請用

```bash
make
```

直接編譯即可

輸出的檔案將會叫作 `main.out`

## 使用者介面

![UI Screenshot](ui-screenshot.png)

使用者介面如圖，先輸入運算式 ，再選擇要操作的功能。選項 4 的求值部份會向使用者要求輸入各個變數的值，詢問順序不保證依序詢問（每個都會問但順序不保證），之後會給出運算結果。

# 實作簡介

核心思想就是用 infix expression + stack 求得一個 expression tree 。

## Prefix & postfix order traversal

利用遞迴方式直接走訪 expression tree 。

## Level-order traversal

透過 BFS 的方式，維護一個 circular queue ，每次都從裡面取最前面的元素出來，然後把他的子節點推進 queue 的尾端，持續執行到 queue 清空為止。

## Evaluation

整體是透過遞迴方式去求值，遞迴的求根節點的位置，然後判斷節點的內容是運算子還是變數，若是變數就詢問，若是運算子就往下求出子節點的值後做運算。 
