#include "expression.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

static int precedence(char ch) {
    if (ch == '+' || ch == '-') {
        return 1;
    } else if (ch == '*' || ch == '/') {
        return 2;
    } else {
        return 0;
    }
}

void free_exp_node(struct exp_node **nd) {
    if (nd == NULL || *nd == NULL)
        return;
    if ((*nd)->l)
        free_exp_node(&((*nd)->l));
    if ((*nd)->r)
        free_exp_node(&((*nd)->r));
    free(*nd);
    *nd = NULL;
}

struct exp_node *expression_infix_ctor(char *s) {
    static int op_top = 0, nd_top = 0;
    static char op_stk[1024] = {0};
    static struct exp_node *nd_stk[1024] = {0};
    for (char *it = s; *it; ++it) {
        if (isalpha(*it)) {
            nd_stk[nd_top] = calloc(1, sizeof(struct exp_node));
            *nd_stk[nd_top] =
                (struct exp_node){.l = NULL, .r = NULL, .val = 0, .c = *it};
            nd_top++;
        } else {

            if (isoperator(*it)) {
                // if is lower precedence
                while (op_top > 0 &&
                       precedence(*it) < precedence(op_stk[op_top - 1])) {
                    if (nd_top < 2) {
                        goto err;
                    }
                    struct exp_node *nd = calloc(1, sizeof(struct exp_node));
                    *nd = (struct exp_node){.l = nd_stk[nd_top - 2],
                                            .r = nd_stk[nd_top - 1],
                                            .val = 0,
                                            .c = op_stk[op_top - 1]};
                    nd_top -= 2;           // pop two element from node
                    op_top--;              // pop old operator
                    nd_stk[nd_top++] = nd; // push new node into node
                }
                op_stk[op_top++] = *it;
            } else if (*it == '(') {
                op_stk[op_top++] = '(';
            } else if (*it == ')') {
                // pop until '(', add safe condition
                for (; op_stk[op_top - 1] != '(' && op_top >= 0; op_top--) {
                    // End of parsing
                    if (nd_top < 2 || op_stk[op_top - 1] == '(') {
                        break;
                    }
                    struct exp_node *nd = calloc(1, sizeof(struct exp_node));
                    *nd = (struct exp_node){.l = nd_stk[nd_top - 2],
                                            .r = nd_stk[nd_top - 1],
                                            .val = 0,
                                            .c = op_stk[op_top - 1]};
                    nd_top -= 2;           // pop two element from node stack
                    nd_stk[nd_top++] = nd; // push new node into node stack
                }
                op_top--; // pop (
            } else {
                // Error detection
                goto err;
            }
        }
    }
    // pop remaining operators
    for (; op_top >= 0; op_top--) {
        // End of parsing
        if (nd_top < 2) {
            break;
        }
        struct exp_node *nd = calloc(1, sizeof(struct exp_node));
        *nd = (struct exp_node){.l = nd_stk[nd_top - 2],
                                .r = nd_stk[nd_top - 1],
                                .val = 0,
                                .c = op_stk[op_top - 1]};
        nd_top -= 2;           // pop two element from node stack
        nd_stk[nd_top++] = nd; // push new node into node stack
    }
    return nd_stk[0];
err:
    // cleanup when error
    for (int i = 0; i < nd_top; ++i) {
        free_exp_node(&nd_stk[i]);
    }
    return NULL;
}

void exp_trav_prefix(struct exp_node *nd) {
    printf("%c", nd->c);
    if (nd->l)
        exp_trav_prefix(nd->l);
    if (nd->r)
        exp_trav_prefix(nd->r);
}

void exp_trav_postfix(struct exp_node *nd) {
    if (nd->l)
        exp_trav_postfix(nd->l);
    if (nd->r)
        exp_trav_postfix(nd->r);
    printf("%c", nd->c);
}

int evaluation(struct exp_node *nd) {
    if (isoperator(nd->c)) {
        switch (nd->c) {
        case '+':
            return evaluation(nd->l) + evaluation(nd->r);
        case '-':
            return evaluation(nd->l) - evaluation(nd->r);
            break;
        case '*':
            return evaluation(nd->l) * evaluation(nd->r);
            break;
        case '/':
            return evaluation(nd->l) / evaluation(nd->r);
            break;
        }
    } else {
        int n;
        printf("Assign %c = ", nd->c);
        scanf("%d", &n);
        return n;
    }
}

void exp_trav_level(struct exp_node *nd) {
    // circular queue
    static struct exp_node *q[1024];
    int q_front = 0, q_rear = 0;
    q[q_rear++] = nd;
    while ((q_front % 1024) != (q_rear % 1024)) {
        struct exp_node *n = q[q_front % 1024];
        printf("%c ", n->c);
        fflush(stdout);
        q_front++; // pop front
        if (n->l) {
            q[q_rear % 1024] = n->l;
            q_rear++;
        }
        if (n->r) {
            q[q_rear % 1024] = n->r;
            q_rear++;
        }
    }
}
