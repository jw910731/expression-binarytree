#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "expression.h"

char buffer[1024];

int main() {
    printf("Input infix representation: ");
    scanf("%1024s", buffer);
    struct exp_node *nd = expression_infix_ctor(buffer);
    if (nd == NULL) {
        fprintf(stderr, "INVALID INPUT\n");
        return EXIT_FAILURE;
    }
    bool flag = true;
    while (flag) {
        int cmd;
        printf("1) Output prefix\n"
               "2) Output postfix\n"
               "3) Output level order traversal\n"
               "4) Evaluate the expression\n"
               "0) Exit\n");
        scanf("%d", &cmd);
        switch (cmd) {
        case 0:
            flag = false;
            break;
        case 1:
            printf("The prefix expression: ");
            exp_trav_prefix(nd);
            break;
        case 2:
            printf("The postfix expression: ");
            exp_trav_postfix(nd);
            break;
        case 3:
            printf("The level-order of the expression tree: ");
            exp_trav_level(nd);
            break;
        case 4:
            printf("Result: %d", evaluation(nd));
            break;
        default:
            printf("Error command!\n");
            break;
        }
        puts("");
    }
    free_exp_node(&nd);
    return EXIT_SUCCESS;
}
