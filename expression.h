#pragma once

#define isoperator(x) ((x) == '+' || (x) == '-' || (x) == '*' || (x) == '/')

struct exp_node {
    struct exp_node *l, *r;
    char c;
    int val; // only valid when c is an alphabet
};

/**
 * @brief Given a string s containing expression and return the corresponding
 * expression tree pointer
 *
 * @param s String with expression of + - * /
 * @return struct exp_node* Expression tree of the given string, NULL when
 * invalidate
 */
struct exp_node *expression_infix_ctor(char *s);

void free_exp_node(struct exp_node **nd);

void exp_trav_prefix(struct exp_node *nd);
void exp_trav_postfix(struct exp_node *nd);

int evaluation(struct exp_node *nd);
void exp_trav_level(struct exp_node *nd);
