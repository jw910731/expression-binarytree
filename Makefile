CC ?= gcc
OBJDIR := $(shell [ -d obj ] || mkdir obj && echo "obj")
CFLAGS += -Wall -Wextra -std=c11
LDFLAGS = 

TARGET = main.out
OBJ = main.o expression.o

.PHONY: all

all: CFLAGS:=$(CFLAGS) -O3
all: $(TARGET) 

debug: CFLAGS:=$(CFLAGS) -g -DDEBUG -fsanitize=leak -fsanitize=undefined
debug: LDFLAGS:=$(LDFLAGS) -fsanitize=address -lubsan -lasan 
debug: $(TARGET)

dev: CFLAGS:=$(CFLAGS) -g -DDEBUG
dev: $(TARGET)

$(TARGET): $(patsubst %, $(OBJDIR)/%, $(OBJ))
	$(CC) $(filter %.o, $^) -o $@ $(LDFLAGS)

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJDIR)/%.d: %.c
	$(CC) -MM -MT $(OBJDIR)/$(<:.c=.o) $(CFLAGS) -o $@ $<

include $(patsubst %.o, $(OBJDIR)/%.d, $(OBJ))

clean:
	rm -rf $(TARGET) $(BUILD_DIR) obj
